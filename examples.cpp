#include "crypto.hpp"
#include <iostream>
#include <vector>


using namespace std;

int main() {
  cout << "SHA-1 with 1 iteration" << endl;
  cout << Crypto::hex(Crypto::sha1("Test")) << endl
       << endl;

  cout << "SHA-1 with two iterations" << endl;
  cout << Crypto::hex(Crypto::sha1(Crypto::sha1("Test"))) << endl;

  cout << "The derived key from the PBKDF2 algorithm" << endl;
  cout << Crypto::hex(Crypto::pbkdf2("Password", "Salt")) << endl;

  cout << "Testing out SHA-256" << endl;
  cout << Crypto::hex(Crypto::sha256("Test")) << endl;

  cout << "Testing out SHA-512" << endl;
  cout << Crypto::hex(Crypto::sha512("Test")) << endl;

  cout << "Testing out MD5" << endl;
  cout << Crypto::hex(Crypto::md5("Test")) << endl;

  cout << "Har kommentert ut koda for å finne passordet, siden en gjennomkjøring tar ca 10 min." << endl;
  cout << "Fant at passordet var QwE." << endl;
  // string pswd = "aaa";
  // string korrekt = "ab29d7b5c589e18b52261ecba1d3a7e7cbf212c6";
  // string key = "";
  // cout << "Prøver å finne passordet til Ola, 3 karakterer" << endl;
  // for (int i = 122; i > 47; i--) {
  //   cout << pswd << endl;
  //   pswd.at(0) = (char)i;
  //   for (int j = 122; j > 47; j--) {
  //     pswd.at(1) = (char)j;
  //     for (int k = 122; k > 47; k--) {
  //       pswd.at(2) = (char)k;
  //       key = Crypto::hex(Crypto::pbkdf2(pswd, "Saltet til Ola", 2048, 160));
  //       if (key.compare(korrekt) == 0) {
  //         cout << pswd << " er passordet til Ola!" << endl;
  //         break;
  //       }
  //     }
  //   }
  // }
  // cout << "Fant ikke passordet til Ola, prøv flere karakterer :l" << endl;
}
